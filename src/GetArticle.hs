{-# LANGUAGE BangPatterns #-}
module GetArticle
  (getArticle,
  wiki2File,
  fileToWiki,
  wa_url,
  wa_links,
  WArticle (WArticleDC)) where

import qualified Data.ByteString.Lazy       as BL
import           Data.ByteString.Lazy.Char8 (pack)
import           Data.List                  (elemIndex, findIndex, foldl',
                                             intersperse, isPrefixOf)
import qualified Data.Text                  as T
import           Data.Text.Lazy             (toStrict)
import qualified Data.Text.Lazy.Encoding    as TLE
import           Network.HTTP.Conduit
import           Prelude                    hiding ()
import           Text.HTML.TagSoup

type TagStr = Tag BL.ByteString
type Text   = T.Text

data WArticle = WArticleDC {
  wa_url   :: !Text
, wa_links :: ![Text]
}

rootUrl :: Text
rootUrl = T.pack "https://en.wikipedia.org"

addRootUrl :: Text -> Text
addRootUrl  = T.append rootUrl

instance Show WArticle where
  show (WArticleDC url links) =
    "U: <" ++ T.unpack url ++ ">\nL:\t<" ++
    T.unpack (T.concat (intersperse (T.pack ">\n\t<") links )) ++ ">\n"

getArticle :: Text -> IO WArticle
getArticle url = putStrLn ( "downlading: " ++ T.unpack  url)
  >>  (simpleHttp . T.unpack . addRootUrl) url
  >>= return .
        WArticleDC url . rmDuplicates . take 14 .
        filterWiki . getLinks . filterEnd . getStart . parseTags

rmDuplicates ::  [Text] -> [Text]
rmDuplicates !xs = foldl' f [] xs
  where f accList new | new `elem` accList  = accList
                      | otherwise           = new:accList

getStart :: [TagStr] -> [TagStr]
!getStart  = dropWhile (not.filterFunc)
  where filterFunc t =
          isTagOpenName (pack "div") t &&
          fromAttrib (pack "class") t == pack "mw-parser-output"

filterEnd :: [TagStr] -> [TagStr]
filterEnd tags = take magicNumber tags
  where
    cumsum = tail . scanl (+) 0 $ map divOpenClosed tags
    magicNumber = case elemIndex 0 cumsum of
      Just n  -> n
      Nothing -> 0
    divOpenClosed t | isTagOpenName  (pack "div") t =  1
                    | isTagCloseName (pack "div") t = -1
                    | otherwise                     =  0

getLinks ::[TagStr] -> [Text]
!getLinks  = map     (toStrict . TLE.decodeUtf8 . (fromAttrib (pack "href")))  .
            filter  (isTagOpenName (pack "a"))

filterWiki :: [Text] -> [Text]
filterWiki !ls = filter fWiki' ls
  where
    fWiki' :: Text -> Bool
    fWiki' !s
      |  T.take 6 s               /= T.pack "/wiki/"        = False
      | (T.take 5  . T.drop 6) s  == T.pack "File:"         = False
      | (T.take 5  . T.drop 6) s  == T.pack "Help:"         = False
      | (T.take 10 . T.drop 6) s  == T.pack "Wikipedia:"    = False
      | T.take 7 s                == T.pack "/wiki/;"       = False
      | otherwise = True

wiki2File :: [WArticle] -> String
wiki2File []    = ""
wiki2File ((WArticleDC !url !links) : xs)  = thisStr ++ wiki2File xs
  where
    thisStr :: String
    !thisStr = "url:"      ++ T.unpack url   ++ "\n" ++
              "links:"    ++ concat (intersperse " " (map T.unpack links)) ++ "\n" ++
              "-"    ++ "\n"

fileToWiki :: [String] -> [WArticle]
fileToWiki [] = []
fileToWiki strLines = thisNode : fileToWiki (tail remainingLines)
  where
    theseLines     = takeWhile (/="-") strLines
    nextLines      = dropWhile (/="-") strLines
    remainingLines = if length nextLines>1 then tail nextLines else nextLines

    thisNode = WArticleDC  url links
      where
        url     = T.pack $ getAttribute "url"  theseLines
        links   = T.words $ T.pack $ getAttribute "links"  theseLines

getAttribute :: String -> [String] -> String
getAttribute key dat = case findIndex (isPrefixOf key) dat of
                         Nothing -> ""
                         Just i  ->  tail $ dropWhile (/= ':') (dat !! i)
