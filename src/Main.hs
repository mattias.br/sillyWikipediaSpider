{-# LANGUAGE BangPatterns #-}
module Main where
import           Control.Monad.State
import           Data.List
import           Data.Text           (Text, pack)
import           GetArticle          (WArticle, getArticle, wa_links)
import           Streaming
import qualified Streaming.Prelude   as S
import           System.IO           (IOMode (WriteMode), hClose, openFile)

type URL = Text

data CrawlState = CrawlState  ![URL]       ![(URL, Int)]
                      --    [Completed]    [(Queue, depth)]

-- Builds list recursively
buildHelper :: CrawlState -> Stream (Of WArticle) IO ()
buildHelper cs@( CrawlState _ queue ) = {-# SCC "buildHelper" #-}
  if null queue
    then return ()
    else do
      (article, cs') <- liftIO (runStateT getNextNode cs)
      S.yield article
      buildHelper cs'

-- State manipulation
getNextNode :: StateT CrawlState IO WArticle
getNextNode = {-# SCC "getNextNode" #-} do
  CrawlState parsed queue@( (url, depth):queueTail ) <- get
  article <- liftIO $ getArticle url
  put $ CrawlState (url:parsed) $ (queueTail++) $  if depth > 1
          then let  newUrls  = wa_links article \\ parsed
                    newUrls' = newUrls          \\ map fst queue
                    in zip newUrls' $ repeat (depth-1)
          else []
  return article

startUrl :: Text
startUrl = pack "/wiki/Haskell_(programming_language)"
filename :: String
filename = "savedArticles.txt"
recursionDepth :: Int
recursionDepth = 3

 -- S.print (buildDB startUrl recursionDepth)
main :: IO ()
main = do outFileHandle <- openFile filename WriteMode
          S.toHandle outFileHandle  . S.show . buildHelper $
              CrawlState [] [(startUrl, recursionDepth)]
          hClose outFileHandle
